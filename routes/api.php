<?php

use App\Helpers\PermissionsHelper as PH;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\QuestController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::group(['middleware' => 'api'],
    function () {


        Route::group(['middleware' => 'auth:api'],
            function () {


                Route::group(['middleware' => ['permission:' . PH::EDIT_AND_VIEW_MY_QUESTS]],
                    function () {
                        Route::post('specialities/{speciality}/quests/', [QuestController::class, 'create']);
                        Route::get('quests/my', [QuestController::class, 'getMyQuest']);
                    });

                Route::group(['middleware' => ['permission:' . PH::EDIT_AND_VIEW_ALL_QUESTS]],
                    function () {
                        Route::get('quests', [QuestController::class, 'index']);
                        Route::get('quests/{quest}', [QuestController::class, 'show']);
                    });


                Route::group(['middleware' => ['permission:' . PH::DUMP_DATA]],
                    function () {
                        Route::post('admin/dump', [AdminController::class, 'dumpData']);
                    });

                Route::group(['middleware' => ['permission:' . PH::CHANGE_USERS_ROLES]],
                    function () {
                        Route::get('admin/users', [AdminController::class, 'userList']);
                        Route::get('admin/roles', [AdminController::class, 'getRoles']);
                        Route::post('admin/users/{user}/role', [AdminController::class, 'setRole']);
                    });


                Route::group(['middleware' => ['permission:' . PH::APPROVE_QUESTS]],
                    function () {
                        Route::post('quests/{quest}/approve', [QuestController::class, 'approveQuest']);
                        Route::post('quests/{quest}/cancel', [QuestController::class, 'cancelApproveQuest']);
                    });

                Route::group(['middleware' => ['permission:' . PH::PUSH_QUESTS]],
                    function () {
                        Route::middleware('can:push,quest')->post('quests/{quest}/push', [QuestController::class, 'pushQuest']);
                    });


                Route::get('documents', [DocumentController::class, 'show']);
                Route::middleware('can:update,quest')->post('specialities/{speciality}/quests/{quest}', [QuestController::class, 'update']);

                Route::get('faculties', [FacultyController::class, 'index']);


            });
        Route::group(['prefix' => 'auth'],
            function () {
                Route::post('login', [AuthController::class, 'login']);
                Route::post('register', [AuthController::class, 'register']);
                Route::post('logout', [AuthController::class, 'logout']);
                //Route::post('refresh',  [AuthController::class, 'refresh']);
                Route::post('me', [AuthController::class, 'me']);
            });

    });

