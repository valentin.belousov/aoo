const ErrorsMixin = {
    props: { errors: { type: Array, default: () => [] } },
    data: () => ({ locErrors: [] }),
    methods: {
        $input() {
            this.locErrors = [];
        },
    },
    watch: {
        errors(value) {
            this.locErrors = value;
        },
    },
};

export default ErrorsMixin;
