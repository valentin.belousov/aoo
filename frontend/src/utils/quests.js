export const initialQuest = {
    faculty: { name: "" },
    speciality: { name: "" },
    speciality_language: { name: "" },
    speciality_basis: { name: "" },
    speciality_forma: { name: "" },
    /**/
    first_name: "",
    passport_series: "",
    children: 0,
    father: false,
    /**/
    documents: [],
    /**/
    country: "",
    locality: "",
    district: "",
    street: "",
    house: "",

    is_approved: false,
};
