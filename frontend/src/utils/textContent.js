import { roles } from "@/utils/gates";

export const textContent = {
    nav: {
        general: "Приемная Комисия Онлайн",
        card: "Анкета",
        card_create: "Создать Анкету",
        card_update: "Отредактировать Анкету",
        card_list: "Все Анкеты",

        admin: "Панель Администратора",
        user_list: "Все Пользователи",
        dump: "Обновление базы данных",
    },
    roles: {
        [roles.ADMINISTRATOR]: "Администратор",
        [roles.MODERATOR]: "Секретарь",
        [roles.USER]: "Абитуриент",
    },
};
