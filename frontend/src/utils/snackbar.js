import state from "@/store";

const snackbar = ({ message }) => {
    state.commit("snackbar/addMsg", message);
};

export default snackbar;
