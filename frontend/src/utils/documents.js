export const documentKeys = {
    passport_first_page: "passport_first_page",
    passport_second_page: "passport_second_page",
};

export const initialQuestDocumentKeys = {
    passport_first_page: { url: null },
    passport_second_page: { url: null },
    resolution: { url: null },
};

export const prepDocuments = (quest) => {
    const { documents = [] } = quest;
    let resDocumetns = {};
    for (const data of documents) {
        resDocumetns[data.name] = data;
    }
    return { ...initialQuestDocumentKeys, ...resDocumetns };
};
