const isJson = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

export const locSKeys = {
    token: "aoo_token",
    role: "aoo_role",
};

export const saveToLocS = (key, value) => {
    window.localStorage.setItem(
        key,
        typeof value !== "object" ? value : JSON.stringify(value)
    );
};

export const removeLocS = (key) => {
    window.localStorage.removeItem(key);
};

export const getFromLocS = (key) => {
    const value = window.localStorage.getItem(key);
    return isJson(value) ? JSON.parse(value) : value;
};
