export const getDiffs = (oldData, data) => {
    let diffs = {};
    for (const [key, value] of Object.entries(data)) {
        if (
            !(
                oldData[key] === value ||
                ([oldData[key], value].includes(null) &&
                    [oldData[key], value].includes(""))
            )
        )
            diffs[key] = value;
    }
    return diffs;
};

export const toFormData = (obj) => {
    const formData = new FormData();
    for (const [key, value] of Object.entries(obj)) {
        formData.set(
            key,
            typeof value === "boolean"
                ? Number(value)
                : value === null
                ? ""
                : value
        );
    }
    return formData;
};

export const prepErrors = (e) => {
    return e.errors || {};
};
