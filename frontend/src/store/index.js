import Vue from "vue";
import Vuex from "vuex";
import app from "@/store/modules/app";
import quests from "@/store/modules/quests";
import admin from "@/store/modules/admin";
import snackbar from "@/store/modules/snackbar";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: { app, quests, admin, snackbar },
});
