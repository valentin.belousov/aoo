import userApi from "@/api/userApi";
import {
    getFromLocS,
    locSKeys,
    removeLocS,
    saveToLocS,
} from "@/utils/localStorage";

const initialState = {
    token: getFromLocS(locSKeys.token) || null,
    user: null,
};

const app = {
    state: initialState,
    getters: {
        isAuth: (state) => state.token !== null && state.user !== null,
        permissions: (state) =>
            state.user?.roles
                .map(({ permissions }) => permissions)
                .flat()
                .map(({ name }) => name) || [],
        roles: (state) => state.user?.roles.map(({ name }) => name) || [],
    },
    mutations: {
        login(state, { access_token, user }) {
            saveToLocS(locSKeys.token, access_token);
            saveToLocS(locSKeys.role, user.roles[0].name);
            state.token = access_token;
            state.user = user;
        },
        logoutMut(state) {
            removeLocS(locSKeys.token);
            removeLocS(locSKeys.role);
            state.token = null;
            state.user = null;
        },
    },
    actions: {
        async login(context, loginData) {
            const authData = await userApi.login(loginData);
            context.commit("login", authData);
        },
        async me(context) {
            const user = await userApi.me();
            context.commit("login", {
                access_token: context.state.token,
                user,
            });
        },
        async register(context, registerData) {
            const authData = await userApi.register(registerData);
            context.commit("login", authData);
        },
        async logout(context) {
            const token = context.state.token;
            context.commit("logoutMut");
            await userApi.logout(token);
        },
    },
    namespaced: true,
};

export default app;
