import adminApi from "@/api/adminApi";

const initialState = {
    serverData: null,
    lastListParams: null,
    roles: [],
};

const admin = {
    state: initialState,
    getters: {},
    mutations: {
        setUserList(state, data) {
            state.serverData = data;
        },
        setRoles(state, data) {
            state.roles = data;
        },
        setLastParams(state, data) {
            state.lastListParams = data;
        },
    },
    actions: {
        async getUserList(context, params) {
            context.commit("setLastParams", params);
            const data = await adminApi.getUserList(params);
            context.commit("setUserList", data);
        },
        async getRoles(context) {
            const data = await adminApi.getRoles();
            context.commit("setRoles", data);
        },
        async setRole(context, { userId, role }) {
            await adminApi.setRole(userId, role);
            await context.dispatch("getUserList", context.state.lastListParams);
        },
    },
    namespaced: true,
};

export default admin;
