import questsApi from "@/api/questsApi";

const initialState = {
    currentQuest: null,
    serverData: null,
};

const quests = {
    state: initialState,
    getters: {
        isExist: (state) => !!state.currentQuest,
    },
    mutations: {
        setCurrentQuest(state, data) {
            state.currentQuest = data;
        },
        setServerData(state, data) {
            state.serverData = data;
        },
    },
    actions: {
        async getMyQuest(context) {
            context.commit("setCurrentQuest", null);
            const data = await questsApi.getMyQuest();
            context.commit("setCurrentQuest", data);
        },
        async getQuest(context, id) {
            context.commit("setCurrentQuest", null);
            const data = await questsApi.getQuest(id);
            context.commit("setCurrentQuest", data);
        },
        async getQuestList(context, params) {
            const data = await questsApi.getQuestList(params);
            context.commit("setServerData", data);
        },
        async createQuest(context, { data, specialityId }) {
            await questsApi.createQuest(specialityId, data);
            await context.dispatch("getMyQuest");
        },

        async updateQuest(context, { data, specialityId, questId, isAdmin }) {
            await questsApi.updateQuest(specialityId, questId, data);
            await context.dispatch(
                isAdmin ? "getQuest" : "getMyQuest",
                questId
            );
        },

        async approveQuest(context, questId) {
            await questsApi.approveQuest(questId);
            await context.dispatch("getQuest", questId);
        },

        async cancelApproveQuest(context, questId) {
            await questsApi.cancelQuest(questId);
            await context.dispatch("getQuest", questId);
        },
    },
    namespaced: true,
};

export default quests;
