import { v1 } from "uuid";

const initialState = {
    msgDelay: 5000,
    messages: [],
};

const quests = {
    state: initialState,

    mutations: {
        addMsg(state, text) {
            state.messages = [...state.messages, { text, id: v1() }];
        },
        removeMsg(state, msgId) {
            state.messages = state.messages.filter(({ id }) => id !== msgId);
        },
    },
    namespaced: true,
};

export default quests;
