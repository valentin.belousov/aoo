import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueGates from "vue-gates";
import YmapPlugin from "vue-yandex-maps";

import VueIco, { icoClose, icoEdit } from "vue-ico";

Vue.config.productionTip = false;

Vue.use(VueGates);
Vue.use(YmapPlugin, {
    apiKey: process.env.VUE_APP_YMAP_KEY,
    lang: "ru_RU",
    coordorder: "latlong",
    version: "2.1",
});

Vue.use(VueIco, {
    close: icoClose,
    edit: icoEdit,
});

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
