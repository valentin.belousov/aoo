import { getFromLocS, locSKeys } from "@/utils/localStorage";

export const syncMiddleware = (to, from, next) => {
    const token = getFromLocS(locSKeys.token);
    const role = getFromLocS(locSKeys.role);

    const isAuth = !!token;
    if (!isAuth && !["Welcome", "Register"].includes(to.name))
        next({ name: "Welcome" });
    else {
        const roles = to.meta.roles || [];

        const isHasRoles = roles.includes(role);
        if (!isHasRoles) next(to.meta.redirect);
        else next();
    }
};
