import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Register from "../views/Register.vue";
import Welcome from "@/views/Home/Welcome";
import QuestView from "@/views/Home/QuestView";
import QuestList from "@/views/Home/QuestList";
import QuestUpdate from "@/views/Home/QuestUpdate";
import Admin from "@/views/Home/Admin";
import Dump from "@/views/Home/Admin/Dump";
import UserList from "@/views/Home/Admin/UserList";
import { syncMiddleware } from "@/router/middleware";
import { roles } from "@/utils/gates";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        component: Home,
        children: [
            {
                path: "/",
                name: "Welcome",
                component: Welcome,
            },
            {
                path: "/card",
                name: "QuestView",
                component: QuestView,
                props: { isAdmin: false },
                meta: { roles: [roles.USER], redirect: { name: "Welcome" } },
            },
            {
                path: "/card/edit",
                name: "QuestUpdate",
                component: QuestUpdate,
                props: { isAdmin: false },
                meta: { roles: [roles.USER], redirect: { name: "Welcome" } },
            },
            {
                path: "/cards",
                name: "QuestList",
                component: QuestList,
                meta: {
                    roles: [roles.ADMINISTRATOR, roles.MODERATOR],
                    redirect: { name: "QuestView" },
                },
            },
            {
                path: "/card/:id",
                name: "AdminQuestView",
                component: QuestView,
                props: { isAdmin: true },
                meta: {
                    roles: [roles.ADMINISTRATOR, roles.MODERATOR],
                    redirect: { name: "Welcome" },
                },
            },
            {
                path: "/card/edit/:id",
                name: "AdminQuestUpdate",
                component: QuestUpdate,
                props: { isAdmin: true },
                meta: {
                    roles: [roles.ADMINISTRATOR, roles.MODERATOR],
                    redirect: { name: "Welcome" },
                },
            },
            {
                path: "/admin",
                name: "Admin",
                component: Admin,

                children: [
                    {
                        meta: {
                            roles: [roles.ADMINISTRATOR],
                            redirect: { name: "Welcome" },
                        },
                        path: "/dump",
                        name: "AdminDump",
                        component: Dump,
                    },
                    {
                        meta: {
                            roles: [roles.ADMINISTRATOR],
                            redirect: { name: "Welcome" },
                        },
                        path: "/users",
                        name: "AdminUserList",
                        component: UserList,
                    },
                ],
            },
        ],
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach(syncMiddleware);

export default router;
