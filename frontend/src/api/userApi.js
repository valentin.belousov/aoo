import BaseApi from "./BaseApi";

class UserApi extends BaseApi {
    register = (registerData) => this.post("/auth/register", registerData);
    login = (loginData) => this.post("/auth/login", loginData);
    logout = (token) =>
        this.post("/auth/logout", {}, { Authorization: `Bearer ${token}` });
    me = () => this.post("/auth/me");
}

export default new UserApi();
