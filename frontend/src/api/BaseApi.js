import axios from "axios";
import { getFromLocS, locSKeys } from "@/utils/localStorage";

const baseURL =process.env.NODE_ENV !== "production" ? process.env.VUE_APP_BASE_URL : window.location.origin;

function request(method, url, data = {}, params = {}, optHeaders = {}) {
    const token = getFromLocS(locSKeys.token);
    return new Promise((resolve, reject) => {
        axios
            .request({
                url,
                method,
                baseURL: `${baseURL}/api`,
                data,
                params,
                headers: { Authorization: `Bearer ${token}`, ...optHeaders },
            })
            .then(({ data }) => resolve(data))
            .catch((e) => reject(e?.response?.data || e));
    });
}

class BaseApi {
    get = (url, params) => request("get", url, {}, params);
    post = (url, data, optHeaders) =>
        request("post", url, data, {}, optHeaders);
    put = (url, data) => request("put", url, data);
    delete = (url, data) => request("delete", url, data);
}

export default BaseApi;
