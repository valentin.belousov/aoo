import BaseApi from "./BaseApi";

class FacultiesApi extends BaseApi {
    getFacultyList = () => this.get(`/faculties`);
}

export default new FacultiesApi();
