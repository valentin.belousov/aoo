import BaseApi from "./BaseApi";
import { toFormData } from "@/utils";

class QuestsApi extends BaseApi {
    getQuestList = (params) => this.get(`/quests`, params);
    getQuest = (id) => this.get(`/quests/${id}`);
    getMyQuest = () => this.get("/quests/my");

    createQuest = (specialityId, data) =>
        this.post(`/specialities/${specialityId}/quests`, toFormData(data));
    updateQuest = (specialityId, questId, data) =>
        this.post(
            `/specialities/${specialityId}/quests/${questId}`,
            toFormData(data)
        );

    approveQuest = (id) => this.post(`/quests/${id}/approve`);
    cancelQuest = (id) => this.post(`/quests/${id}/cancel`);

    pushQuest = (id) => this.post(`/quests/${id}/push`);
}

export default new QuestsApi();
