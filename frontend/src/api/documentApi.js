import BaseApi from "./BaseApi";

class DocumentApi extends BaseApi {
    getDocument = (url) => this.get(`/documents`, { url });
}

export default new DocumentApi();
