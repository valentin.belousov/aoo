import BaseApi from "./BaseApi";
import { toFormData } from "@/utils";

class AdminApi extends BaseApi {
    dump = (data) => this.post("/admin/dump", toFormData(data));
    getUserList = (params) => this.get("/admin/users", params);
    getRoles = () => this.get("/admin/roles");
    setRole = (id, role) => this.post(`/admin/users/${id}/role`, { role });
}

export default new AdminApi();
