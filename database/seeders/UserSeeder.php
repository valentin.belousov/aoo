<?php

namespace Database\Seeders;

use App\Helpers\RolesHelper as RH;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return string
     */
    static function getDefPass(): string
    {
        return Hash::make(config('app.default_user_password'));
    }

    public function run()
    {
        $admins = [];

        $moders = [];

        $users = [];

        for ($i = 0; $i < 16; $i++) {


            array_push($admins, User::factory()->create([
                'email' => "admin" . $i . "@test.com",
                'password' => static::getDefPass(),
            ]));

            array_push($moders, User::factory()->create([
                'email' => "moder" . $i . "@test.com",
                'password' => static::getDefPass(),
            ]));

            array_push($users, User::factory()->create([
                'email' => "user" . $i . "@test.com",
                'password' => static::getDefPass(),
            ]));
        }


        foreach ($admins as $admin) {
            $admin->setRole([RH::ADMINISTRATOR]);
        }

        foreach ($moders as $moder) {
            $moder->setRole([RH::MODERATOR]);
        }

        foreach ($users as $user) {
            $user->setRole([RH::USER]);
        }
    }
}
