<?php

namespace Database\Seeders;

use Illuminate\Filesystem\Filesystem;
use App\Helpers\DocumentHelper as DH;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,

            UserSeeder::class,

            QuestSeeder::class,

            DocumentSeeder::class
        ]);
    }
}
