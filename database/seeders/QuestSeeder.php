<?php

namespace Database\Seeders;

use App\Helpers\RolesHelper as RH;
use App\Models\Faculty;
use App\Models\Quest;
use App\Models\User;
use Illuminate\Database\Seeder;

class QuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::role(RH::USER)->get();

        $faculties = Faculty::all();


        foreach ($users as $user) {

            $faculty = $faculties->random();
            $specialities = $faculty->specialities()->get();
            $speciality = count($specialities) ? $specialities->random() : null;
            $languages = $speciality ? $speciality->languages()->get() : [];

            $language = count($languages) ? $languages->random() : null;

            $basises = $speciality ? $speciality->basises()->get() : [];
            $basis = count($basises) ? $basises->random() : null;

            $formas = $speciality ? $speciality->formas()->get() : [];
            $forma = count($formas) ? $formas->random() : null;


            Quest::factory()->create([
                'user_id' => $user->id,

                'faculty_id' => $faculty ? $faculty->id : null,
                'speciality_id' => $speciality ? $speciality->id : null,
                'speciality_forma_id' => $forma ? $forma->id : null,
                'speciality_basis_id' => $basis ? $basis->id : null,
                'speciality_language_id' => $language ? $language->id : null,
            ]);
        }
    }
}
