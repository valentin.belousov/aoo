<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Helpers\RolesHelper as RH;
use App\Helpers\PermissionsHelper as PH;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            PH::EDIT_AND_VIEW_MY_QUESTS,
            PH::EDIT_AND_VIEW_ALL_QUESTS,
            PH::APPROVE_QUESTS,
            PH::DUMP_DATA,
            PH::CHANGE_USERS_ROLES,
            PH::PUSH_QUESTS
        ];

        $roles = [
            RH::ADMINISTRATOR => [
                PH::EDIT_AND_VIEW_ALL_QUESTS,
                PH::APPROVE_QUESTS,
                PH::DUMP_DATA,
                PH::CHANGE_USERS_ROLES,
                PH::PUSH_QUESTS,
            ],
            RH::MODERATOR => [
                PH::EDIT_AND_VIEW_ALL_QUESTS,
                PH::APPROVE_QUESTS,
                PH::PUSH_QUESTS,
            ],
            RH::USER => [
                PH::EDIT_AND_VIEW_MY_QUESTS,
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::create(
                [
                    'name' => $permission,
                    'guard_name' => 'api'
                ]
            );
        }

        foreach ($roles as $key => $rolePers) {
            /** @var Role $role */
            $role = Role::create(['name' => $key, 'guard_name' => 'api']);
            $role->syncPermissions($rolePers);
        }
    }
}
