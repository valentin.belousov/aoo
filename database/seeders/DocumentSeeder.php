<?php

namespace Database\Seeders;

use App\Helpers\DocumentHelper as DH;
use App\Models\Quest;
use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk(DH::MEDIA_DISK)->deleteDirectory(DH::FOLDER);
        Storage::disk(DH::MEDIA_DISK)->deleteDirectory(DH::APPROVE_FOLDER);

        $quests = Quest::all();

        foreach ($quests as $quest) {
            foreach (DH::DOCUMENT_KEYS as $key) {

                $quest->documents()->create([
                    'url' => DH::saveImage('https://picsum.photos/250', "jpg", $quest->id, $key),
                    'title' => DH::DOCUMENT_TITLES[$key],
                    'name' => $key,
                ]);
            };
        }
    }
}
