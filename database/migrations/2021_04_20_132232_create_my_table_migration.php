<?php

use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMyTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(Storage::disk('dump')->get("db.sql"));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allset');
        Schema::dropIfExists('basis');
        Schema::dropIfExists('faculty');
        Schema::dropIfExists('forma');
        Schema::dropIfExists('language');
        Schema::dropIfExists('speciality');
    }
}
