<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');


            $table->unsignedBigInteger('faculty_id')->nullable();
            $table->unsignedBigInteger('speciality_id')->nullable();
            $table->unsignedBigInteger('speciality_forma_id')->nullable();
            $table->unsignedBigInteger('speciality_basis_id')->nullable();
            $table->unsignedBigInteger('speciality_language_id')->nullable();

            $table->string('first_name');
            $table->string('passport_series');
            $table->integer('children');
            $table->boolean('father');

            $table->string('country');
            $table->string('locality');
            $table->string('district')->nullable();
            $table->string('street')->nullable();
            $table->string('house')->nullable();

            $table->boolean('is_approved')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quests');
    }
}
