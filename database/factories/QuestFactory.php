<?php

namespace Database\Factories;

use Illuminate\Support\Facades\Hash;

use App\Models\Quest;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Crypt;

class QuestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Quest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->name(),
            'children' => $this->faker->randomNumber(),
            'father' => $this->faker->boolean(),
            'passport_series' => Crypt::encryptString('AB123'),

            'country' => 'Приднестровье',
            'locality' => 'Бендеры',
            'district' => 'микрорайон Борисовка',
            'street' => 'Бельцкая',
            'house' => '27',
        ];
    }
}
