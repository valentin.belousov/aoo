<?php

namespace App\Helpers;

/**
 * Class RolesHelper
 * @package App\Helpers
 */
class RolesHelper
{
    const ADMINISTRATOR = 'ADMINISTRATOR';
    const MODERATOR = 'MODERATOR';
    const USER = 'USER';
}
