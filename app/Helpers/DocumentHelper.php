<?php

namespace App\Helpers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

/**
 * Class RolesHelper
 * @package App\Helpers
 */
class DocumentHelper
{
    const passport_first_page = 'passport_first_page';
    const passport_second_page = 'passport_second_page';
    const resolution = 'resolution';

    const DOCUMENT_KEYS = [
        self::passport_first_page,
        self::passport_second_page,
        self::resolution
    ];

    const DOCUMENT_TITLES = [
        self::resolution => 'Разрешение на обработку данных',
        self::passport_first_page => 'Первая страница паспорта',
        self::passport_second_page => 'Вторая страница паспорта',
    ];

    const MEDIA_DISK = 'local';

    const FOLDER = 'documents';

    const APPROVE_FOLDER = 'approved';

    public static function saveImage(string $fileSource, string $ext, int $questId, string $documentKey, string $replacedUrl = null): string
    {
        $file = file_get_contents($fileSource);
        $fileName = self::FOLDER . "/" . $questId . "/" . $documentKey . '.' . $ext;

        if ($replacedUrl) Storage::disk(self::MEDIA_DISK)->delete($replacedUrl);

        Storage::disk(self::MEDIA_DISK)->put($fileName, $file);

        return $fileName;
    }

}

{
}
