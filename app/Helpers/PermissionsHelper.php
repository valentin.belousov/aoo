<?php

namespace App\Helpers;

/**
 * Class PermissionsHelper
 * @package App\Helpers
 */
class PermissionsHelper
{
    const EDIT_AND_VIEW_MY_QUESTS = 'EDIT_AND_VIEW_MY_QUESTS';

    const EDIT_AND_VIEW_ALL_QUESTS = 'EDIT_AND_VIEW_ALL_QUESTS';

    const APPROVE_QUESTS = 'APPROVE_QUESTS';

    const PUSH_QUESTS = 'PUSH_QUESTS';

    const DUMP_DATA = 'DUMP_DATA';

    const CHANGE_USERS_ROLES = 'CHANGE_USERS_ROLES';
}
