<?php

namespace App\Policies;

use App\Helpers\PermissionsHelper as PH;
use App\Models\Quest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function view(User $user, Quest $quest)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function push(User $user, Quest $quest)
    {
        return $quest->is_approved;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function update(User $user, Quest $quest)
    {
        return !$quest->is_approved && ($quest->user_id === $user->id || $user->hasAllPermissions([PH::EDIT_AND_VIEW_ALL_QUESTS]));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function delete(User $user, Quest $quest)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function restore(User $user, Quest $quest)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Quest $quest
     * @return mixed
     */
    public function forceDelete(User $user, Quest $quest)
    {
        //
    }
}
