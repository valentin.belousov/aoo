<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    use HasFactory;

    protected $table = 'speciality';

    public function languages()
    {
        return $this->belongsToMany(Language::class, 'allset', 'speciality_id', 'language_id')->distinct();
    }


    public function basises()
    {
        return $this->belongsToMany(Basis::class, 'allset', 'speciality_id', 'basis_id')->distinct();
    }

    public function formas()
    {
        return $this->belongsToMany(Forma::class, 'allset', 'speciality_id', 'forma_id')->distinct();
    }
}
