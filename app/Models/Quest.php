<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quest extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'faculty_id',
        'speciality_id',
        'speciality_forma_id',
        'speciality_basis_id',
        'speciality_language_id',
        /**/
        'first_name',
        'passport_series',
        'children',
        'father',
        /**/
        'country',
        'locality',
        'district',
        'street',
        'house',
        /**/
        'is_approved',
    ];

    public function faculty()
    {
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }

    public function speciality()
    {
        return $this->belongsTo(Speciality::class, 'speciality_id');
    }

    public function speciality_forma()
    {
        return $this->belongsTo(Forma::class, 'speciality_forma_id');
    }

    public function speciality_basis()
    {
        return $this->belongsTo(Basis::class, 'speciality_basis_id');
    }

    public function speciality_language()
    {
        return $this->belongsTo(Language::class, 'speciality_language_id');
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
