<?php

namespace App\Http\Requests\Quests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateQuestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $speciality = $this->route('speciality');
        $id = $speciality->id;
        return [
            'faculty_id' => ['required', 'int', Rule::exists('speciality', 'faculty_id')->where('id', $id)],
            'speciality_forma_id' => ['nullable', 'int', Rule::exists('allset', 'forma_id')->where('speciality_id', $id)],
            'speciality_basis_id' => ['nullable', 'int', Rule::exists('allset', 'basis_id')->where('speciality_id', $id)],
            'speciality_language_id' => ['nullable', 'int', Rule::exists('allset', 'language_id')->where('speciality_id', $id)],
            /**/
            'first_name' => 'required|string',
            'passport_series' => 'required|string',
            'father' => 'required|boolean',
            'children' => 'required|integer|min:0',
            /**/
            'country' => 'required|string',
            'locality' => 'required|string',
            'district' => 'nullable|string',
            'street' => 'nullable|string',
            'house' => 'nullable|string',
            /**/
            'resolution' => 'required|file:jpg,jpeg,png',
            'passport_first_page' => 'required|file:jpg,jpeg,png',
            'passport_second_page' => 'required|file:jpg,jpeg,png',
        ];
    }
}
