<?php

namespace App\Http\Requests\Quests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UpdateQuestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $speciality = $this->route('speciality');
        $id = $speciality->id;
        return [
            'faculty_id' => ['required', 'int', Rule::exists('speciality', 'faculty_id')->where('id', $id)],
            'speciality_forma_id' => ['nullable', 'int', Rule::exists('allset', 'forma_id')->where('speciality_id', $id)],
            'speciality_basis_id' => ['nullable', 'int', Rule::exists('allset', 'basis_id')->where('speciality_id', $id)],
            'speciality_language_id' => ['nullable', 'int', Rule::exists('allset', 'language_id')->where('speciality_id', $id)],
            /**/
            'first_name' => 'string',
            'passport_series' => 'string',
            'father' => 'boolean',
            'children' => 'integer|min:0',
            /**/
            'country' => 'string',
            'locality' => 'string',
            'district' => 'nullable|string',
            'street' => 'nullable|string',
            'house' => 'nullable|string',
            /**/
            'passport_first_page' => 'file:jpg,jpeg,png',
            'passport_second_page' => 'file:jpg,jpeg,png',
        ];
    }
}
