<?php

namespace App\Http\Controllers;

use App\Http\Requests\Quests\CreateQuestRequest;
use App\Http\Requests\Quests\UpdateQuestRequest;
use Illuminate\Support\Arr;
use App\Models\Quest;
use App\Models\Speciality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\DocumentHelper as DH;
use Illuminate\Support\Facades\Storage;

class QuestController extends Controller
{
    const QUEST_RELATIONS = ['faculty', 'user', 'speciality', 'speciality_forma', 'speciality_basis', 'speciality_language', 'documents'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const CRYPT_KEYS = ['passport_series'];

    public function decryptValues(array $quest = null)
    {
        if ($quest === null) return null;
        foreach (self::CRYPT_KEYS as $key) {
            $quest[$key] = Crypt::decryptString($quest[$key]);
        }
        return $quest;
    }

    public function approveQuest(Quest $quest)
    {
        $fromFolder = DH::FOLDER . "/" . $quest->id;

        $toFolder = DH::APPROVE_FOLDER . "/" . $quest->id;

        $documents = $quest->documents()->get();

        Storage::disk(DH::MEDIA_DISK)->makeDirectory($toFolder);

        foreach ($documents as $document) {

            $ext = pathinfo($document->url)['extension'];

            $fromFile = $fromFolder . "/" . $document->name . '.' . $ext;
            $toFile = $toFolder . "/" . $document->name . '.' . $ext;

            Storage::disk(DH::MEDIA_DISK)->copy($fromFile, $toFile);

        }

        return response($quest->fill(['is_approved' => true])->save());
    }

    public function cancelApproveQuest(Quest $quest)
    {
        $folder = DH::APPROVE_FOLDER . "/" . $quest->id;
        Storage::disk(DH::MEDIA_DISK)->deleteDirectory($folder);

        return response($quest->fill(['is_approved' => false])->save());
    }

    public function pushQuest(Quest $quest)
    {
        return response(Arr::except($this->decryptValues($quest->toArray()), ['is_approved', 'created_at', 'updated_at']));
    }


    public function encryptValues(array $quest = null, array $validated = [])
    {
        if ($quest === null) return null;
        foreach (self::CRYPT_KEYS as $key) {
            if (array_key_exists($key, $validated)) $quest[$key] = Crypt::encryptString($validated[$key]);
        }
        return $quest;
    }

    public function index(Request $request)
    {

        $faculty = $request->query('faculty');
        $first_name = $request->query('first_name');
        $is_approved = $request->query('is_approved');

        return response(
            Quest::query()
                ->where([
                    ['first_name', "like", "%" . $first_name . "%"],
                    $faculty ? ['faculty_id', $faculty] : [null],
                    ['is_approved', $is_approved]
                ])
                ->with(self::QUEST_RELATIONS)->paginate()
        );
    }

    public function getMyQuest(Request $request)
    {
        $data = $request->user()->quest()->with(self::QUEST_RELATIONS)->first();
        return response($data === null ? null : $this->decryptValues($data->toArray()));
    }

    public function show(Quest $quest)
    {
        $data = Quest::query()->with(self::QUEST_RELATIONS)->find($quest->id)->toArray();
        return response($this->decryptValues($data));
    }

    public function create(Speciality $speciality, CreateQuestRequest $request)
    {
        $validated = $request->validated();
        $data = $this->encryptValues(array_merge($validated, ['speciality_id' => $speciality->id]), $validated);

        $quest = $request->user()->quest()->create($data);

        foreach (DH::DOCUMENT_KEYS as $key) {
            $file = $request->file($key);
            $quest->documents()->create([
                'url' => DH::saveImage($file, $file->getClientOriginalExtension(), $quest->id, $key),
                'title' => DH::DOCUMENT_TITLES[$key],
                'name' => $key,
            ]);
        }


        return response($quest);
    }


    public function update(Speciality $speciality, Quest $quest, UpdateQuestRequest $request)
    {
        $validated = $request->validated();
        $data = $this->encryptValues(array_merge($validated, ['speciality_id' => $speciality->id]), $validated);

        $documents = $quest->documents()->get();

        foreach ($documents as $document) {
            $key = $document->name;
            $file = $request->file($document->name);
            if ($file) {
                $document->fill([
                    'url' => DH::saveImage($file, $file->getClientOriginalExtension(), $quest->id, $key, $document->url),
                ])->save();
            }
        }

        return response($quest->fill($data)->save());
    }
}
