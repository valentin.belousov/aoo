<?php

namespace App\Http\Controllers;

use App\Models\Faculty;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Faculty::query()->with(['specialities', 'specialities.languages', 'specialities.basises', 'specialities.formas'])->get());
    }

}
