<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper as DH;

use App\Helpers\RolesHelper as RH;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    public function show(Request $request)
    {
        $validated = $request->validate([
            'url' => 'required|string',
        ]);

        $url = $validated['url'];

        $user = $request->user();


        $quest = $user->quest()->first();
        $isMyDocument = $quest ? !!count($quest->documents()->where(['url' => $url])->get()) : false;

        if (($user->hasRole([RH::ADMINISTRATOR, RH::MODERATOR]) || $isMyDocument)) {
            $file = Storage::disk(DH::MEDIA_DISK)->get($url);
            return response(base64_encode($file));
        }

        return response('You do not have rights to this image.', 403);
    }
}
