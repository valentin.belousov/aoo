<?php

namespace App\Http\Controllers;


use App\Helpers\DocumentHelper as DH;
use App\Http\Requests\Admin\DumpRequest;
use App\Models\Document;
use App\Models\Quest;
use App\Models\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    public function setRole(User $user, Request $request)
    {

        $validated = $request->validate([
            'role' => 'required|int',
        ]);

        if ($request->user()->id === $user->id) return response('You cannot edit your own role.', 403);

        $role = $validated['role'];

        $user->setRole([$role]);

    }

    public function userList(Request $request)
    {
        $role = $request->query('role');
        $email = $request->query('email');
        $name = $request->query('name');

        return response(User::query()
            ->whereExists(function ($query) use ($role) {
                if ($role) $query
                    ->from('model_has_roles')
                    ->where('model_has_roles.role_id', '=', $role)
                    ->whereColumn('model_has_roles.model_id', 'users.id');
            })
            ->where('email', 'like', '%' . $email . '%')
            ->where('name', 'like', '%' . $name . '%')
            ->with('roles.permissions', 'quest')->paginate());

    }

    public function getRoles()
    {
        return response(Role::all());
    }

    public function dumpData(DumpRequest $request)
    {
        $validated = $request->validated();

        if (!Hash::check($validated['password'], $request->user()->password)) return response(['message' => 'Неверный пароль.'], 403);


        DB::table('allset')->delete();
        DB::table('basis')->delete();
        DB::table('faculty')->delete();
        DB::table('forma')->delete();
        DB::table('language')->delete();
        DB::table('speciality')->delete();

        DB::table('quests')->delete();

        $file = $request->file('file');

        DB::unprepared(file_get_contents($file));

        Storage::disk('dump')->delete("db.sql");
        Storage::disk('dump')->put("db.sql", file_get_contents($file));

        Storage::disk(DH::MEDIA_DISK)->deleteDirectory(DH::FOLDER);

        return response(true);
    }

}
